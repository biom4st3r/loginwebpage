import React from "react";
import { COOKIES } from "..";

interface PrivateProps {
    authenticated: JSX.Element
    unauthenticated: JSX.Element
}

export class Private extends React.PureComponent<PrivateProps, any> {
    
    render(): React.ReactNode {
        return <>{'wowacookie' in COOKIES ? this.props.authenticated : this.props.unauthenticated}</>
    }
}