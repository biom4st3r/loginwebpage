import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { Private } from './ts/PrivateElement';

export var COOKIES: Record<string,string>

function initCookies() {
    COOKIES = document.cookie.length !== 0 ? document.cookie
    .split(';')
    .map(entry => entry.split('='))
    .reduce((acc, v) => {
        acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(v[1].trim())
        return acc;
    }, {} as Record<string,string>) : {}
}

initCookies()

class Index extends React.Component {
    render(): React.ReactNode {
        return <div>
            <Private unauthenticated={<Login/>} authenticated={<span>LOGGED IN! wowacookie: {COOKIES.wowacookie}</span>}/>
        </div>
  }
}

// function addCookie(name: string, value: string, expire: Date) {
//     document.cookie += `${name}=${value}; expires=${expire}; SameSite=Strict`
//     initCookies()
// }

interface LoginState {
    register: boolean
}

class Login extends React.Component<any, LoginState> {
    state = {register: false} as LoginState

    login(): JSX.Element {
        return <>
        <form onSubmit={async event => {
                event.preventDefault()
                let ct = event.currentTarget as any
                let body = {username: ct.username.value, password: ct.password.value, remember_me: ct.remember_me.checked}
                let response = await fetch('http://localhost:8000/woobalooo', {method: 'POST', body: JSON.stringify(body), headers: {'Content-Type': 'application/json'}})
                let json: Record<string,any> = await response.json()
                if ('success' in json) {
                    document.cookie += json.success;
                    initCookies();
                    window.location.reload();
                }
                console.log(json)
                // if (response.redirected) {
                //     document.open(response.url)
                // }
            }}>
                <div>
                    <span>Username </span>
                    <input type={'text'} name='username'></input>
                </div>
                <div>
                    <span>Password </span>
                    <input type={'password'} name='password'></input>
                </div>
                <div>
                    <span>Remember Me </span>
                    <input defaultChecked={true} type={'checkbox'} name={'remember_me'}></input>
                </div>
                <button>Login</button>
            </form>
            <button onClick={event => {
                event.preventDefault()
                this.setState({register: true})
            }}>Register</button>
        </>
    }

    register() {
        return <form onSubmit={async event => {
            event.preventDefault()
            let ct = event.currentTarget as any
            let body = {username: ct.username.value, password: ct.password.value, email: ct.email.value}
            console.log(body)
            let response = await fetch('http://localhost:8000/woobaliii', {method: 'POST', body: JSON.stringify(body), headers: {'Content-Type': 'application/json'}})
            let json = await response.json()
            console.log(json)
            this.setState({register: false})
        }}>
            <div>
                <span>Username </span>
                <input type={'text'} name='username'></input>
            </div>
            <div>
                <span>Email </span>
                <input type={'email'} name='email'></input>
            </div>
            <div>
                <span>Password </span>
                <input type={'password'} name='password'></input>
            </div>
            <button>Register</button>
        </form>
    }

    render(): React.ReactNode {
        return <div>
            {this.state.register ? this.register() : this.login()}
        </div>
    }
}

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <Index/>

);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
