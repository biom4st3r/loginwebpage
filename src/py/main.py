import math
from time import perf_counter_ns
import time
from typing import Any
from uuid import uuid4
from fastapi import FastAPI, Response
from pydantic import BaseModel
from pymongo import MongoClient
import bcrypt
from datetime import datetime
from dateutil.relativedelta import relativedelta
from fastapi.middleware.cors import CORSMiddleware
from dotenv import load_dotenv
from os import environ as env


def datePlus1Month():
    return datetime.now() + relativedelta(months=1)


def isExpired(date: str):
    return datetime.now() > datetime.fromisoformat(date)


load_dotenv()
DB_NAME = env.get('DB_NAME')
COLLECTION_NAME = env.get('DB_COLLECTION')
COOKIE_NAME = 'wowacookie'

URL = env.get('DB_URL')
USERNAME = env.get('DB_USERNAME')
PASSWORD = env.get('DB_PASSWORD')
client = MongoClient(f'mongodb+srv://{USERNAME}:{PASSWORD}@{URL}/?retryWrites=true&w=majority')

db = client[DB_NAME]
collection = db[COLLECTION_NAME]

ACCOUNT_SERVER = FastAPI()
origins = [
    'http://localhost:8000',
    'http://127.0.0.1:8000',
    'http://127.0.0.1:3000',
    'http://localhost:3000',
    'http://10.0.0.70:3000',
]


ACCOUNT_SERVER.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['POST', 'GET', 'PUT'],
    allow_headers=["*"],
    max_age=3600,
)


class LoginPost(BaseModel):
    username: str
    password: str
    remember_me: bool


class CreateAccountPost(BaseModel):
    username: str
    password: str
    email: str


class CookieEntry(BaseModel):
    cookie: str
    expire: str


class User(BaseModel):
    usr: str
    pwd: bytes
    email: str
    cookie: CookieEntry


def wait(start: int, response: Any):
    '''Standardize the time it takes to login. Prevent side channel timing attacks'''
    duration = ((perf_counter_ns() - start) / 1_000_000_000)
    time.sleep((math.ceil(duration * 2) / 2) - duration)
    return response


@ACCOUNT_SERVER.post('/woobaliii')
async def register(post: CreateAccountPost, response: Response):
    START = perf_counter_ns()
    username_lookup = collection.find_one({'usr': post.username})
    print(username_lookup)
    email_lookup = collection.find_one({'email': post.email})
    if username_lookup:
        return wait(START, {'error': 'username taken'})
    if email_lookup:
        return wait(START, {'error': 'email taken'})
    uuid = str(uuid4())
    collection.insert_one({
        'usr': post.username,
        'pwd': bcrypt.hashpw(post.password.encode('utf-8'), bcrypt.gensalt()),
        'email': post.email,
        'cookie': {
            'cookie': uuid,
            'expire': str(datePlus1Month())
        }
    })
    return wait(START, {'success': '<3'})


@ACCOUNT_SERVER.post('/woobalooo')
async def login(post: LoginPost, response: Response):
    START = perf_counter_ns()
    lookup = collection.find_one({'usr': post.username})

    if not lookup:  # User doesn't exist
        return wait(START, {'error': 'failed'})
    print(lookup)
    user: User = User.parse_obj(lookup)

    if not bcrypt.checkpw(post.password.encode('utf-8'), user.pwd):  # Wrong password
        return wait(START, {'error': 'failed'})
    # Successful login: extend cookie expiration
    newdate = datePlus1Month()

    if isExpired(user.cookie.expire):
        uuid = str(uuid4())
        entry = {'cookie': {
            'cookie': uuid,
            'expire': str(newdate),
        }}
    else:
        uuid = user.cookie.cookie
        entry = {'cookie': {
            'cookie': uuid,
            'expire': str(newdate),
        }}
    collection.update_one({'usr': post.username}, {
        '$set': entry
    })
    response.set_cookie(key=COOKIE_NAME, value=uuid)
    f = newdate.strftime("%a, %d %b %Y %H:%M:%S GMT") if post.remember_me else "session"
    print(f)
    print(post.remember_me)
    return wait(START, {'success': f'{COOKIE_NAME}={uuid}; expires={newdate.strftime("%a, %d %b %Y %H:%M:%S GMT") if post.remember_me else "session"}; SameSite=Strict'})


# while True:
#     choice = input('''
#     1. Create account
#     2. Login
#     3. Change password
#     -> ''')
#     match choice:
#         case '1':
#             u = input('new username: ').lower()
#             entry = collection.find_one({'usr': u})
#             while entry:
#                 print('ERROR: username is taken.')
#                 u = input('new username: ').lower()
#                 entry = collection.find_one({'usr': u})
#             # print(data)
#             p = input('new password: ')
#             result = collection.insert_one({'usr': u, 'pwd': bcrypt.hashpw(p.encode('utf-8'), bcrypt.gensalt())})
#             print(result)
#         case '2':
#             u = input('username: ').lower()
#             entry = collection.find_one({'usr': u})
#             while not entry:
#                 print('ERROR: username not found')
#                 u = input('username: ').lower()
#                 entry = collection.find_one({'usr': u})
#             p = input('password: ')
#             if bcrypt.checkpw(p.encode('utf-8'), entry['pwd']):
#                 print(f'welcome {u}')
#             else:
#                 print('invalid password')
#         case '3':
#             u = input('username: ').lower()
#             entry = collection.find_one({'usr': u})
#             while not entry:
#                 print('ERROR: username not found')
#                 u = input('username: ').lower()
#                 entry = collection.find_one({'usr': u})
#             p = input('password: ')
#             if bcrypt.checkpw(p.encode('utf-8'), entry['pwd']):
#                 p = input('new password: ')
#                 collection.update_one({'usr': u}, {
#                     '$set': {'pwd': bcrypt.hashpw(p.encode('utf-8'), bcrypt.gensalt())}
#                 })
#                 print('success!')
#             else:
#                 print('invalid password')
